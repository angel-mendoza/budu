import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: 'Angel',
    lastname: 'Mendoza',
    pokemon: {}
  },
  getters: {
    fullName (state) {
      return `${state.name} ${state.lastname}`
    },
    getPokemon (state) {
      return state.pokemon
    }
  },
  mutations: {
    changeFullName (state, payload) {
      state.name = payload.name
      state.lastname = payload.lastname
    },
    changePokemon (state, payload) {
      return state.pokemon = payload
    }
  },
  actions: {
    async getYourPokemon ({commit}, payload) {
      try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${payload}/`)
        commit('changePokemon', res.data)
      } catch (err) {
        console.error("Error response")
      }
    }
  },
})