
const page = path => () => import(`../views/${path}`).then(m => m.default || m)

const routes = [
  { path: '/', name: 'Home',  component: page('home') },
]


export default routes